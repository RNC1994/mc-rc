package RC2K7.Plugins.RPGAPI.Util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;

public final class MultiColorUtil {
	
	private static HashMap<String, String> ColorMap = new HashMap<>();
	static
	{
		for(ChatColor color : ChatColor.values())
		{
			ColorMap.put("{" + color.name().toLowerCase() + "}", color.toString());
		}
	}
	
	public static String colorText(String message, Object ... args)
	{
		for(Entry<String, String> entry : ColorMap.entrySet())
		{
			message = message.replace(entry.getKey(), entry.getValue());
		}
		if(args.length > 0)
		{
			return String.format(message, args);
		}
		return message;
	}
	
	public static void send(CommandSender sender, String message, Object ... args)
	{
		sender.sendMessage(colorText(message, args));
	}
	
	public static void send(String name, String message, Object ... args)
	{
		PlayerUtil.getPlayer(name).sendMessage(colorText(message, args));
	}
        
        public static List<ChatColor> ColorList = new ArrayList<>();
        static
        {
            ColorList.add(ChatColor.AQUA);
            ColorList.add(ChatColor.BLACK);
            ColorList.add(ChatColor.BLUE);
            ColorList.add(ChatColor.DARK_AQUA);
            ColorList.add(ChatColor.DARK_BLUE);
            ColorList.add(ChatColor.DARK_GRAY);
            ColorList.add(ChatColor.DARK_GREEN);
            ColorList.add(ChatColor.DARK_PURPLE);
            ColorList.add(ChatColor.DARK_RED);
            ColorList.add(ChatColor.GOLD);
            ColorList.add(ChatColor.GRAY);
            ColorList.add(ChatColor.GREEN);
            ColorList.add(ChatColor.LIGHT_PURPLE);
            ColorList.add(ChatColor.MAGIC);
            ColorList.add(ChatColor.RED);
            ColorList.add(ChatColor.WHITE);
            ColorList.add(ChatColor.YELLOW);
        }
        
        public static String ArcaneLang(String common)
        {
            StringBuilder sb = new StringBuilder();
            sb.append(ChatColor.MAGIC);
            for(char character : common.toCharArray())
            {
                sb.append(ColorList.get(new Random().nextInt(ColorList.size()))).append(character);
            }
            return sb.toString();
        }

}
