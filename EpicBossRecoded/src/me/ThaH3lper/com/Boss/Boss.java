package me.ThaH3lper.com.Boss;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;

public class Boss
{
  private String name;
  private String entityspawnname;
  private String Timer;
  private int MaxHealth;
  private int Health;
  private LivingEntity entity;
  private int damage;
  private boolean showHp;
  private Location spawnlocation;
  private Location savedlocation;
  private boolean saved = true;
  private List<String> Items;
  private List<String> Skills;
  private List<Integer> percent;
  private List<String> Effects;
  private String skin;
  private Boolean showtitle;
  private Boolean naturalSpawned;

  public Boss(String newname, int newmaxhealth, Location newspawnlocation, String newentityspawnname, int newdamage, boolean newshowHP, List<String> newItems, List<String> newSkills, boolean newshowTitle, String newskin)
  {
    this.name = newname;
    this.MaxHealth = newmaxhealth;
    this.Health = newmaxhealth;
    this.damage = newdamage;
    this.showHp = newshowHP;
    this.spawnlocation = newspawnlocation;
    this.savedlocation = newspawnlocation;
    this.entityspawnname = newentityspawnname;
    this.Items = new ArrayList(newItems);
    this.Skills = new ArrayList(newSkills);
    this.percent = new ArrayList();
    this.Effects = AddSkills();
    this.Timer = "null";
    this.naturalSpawned = Boolean.valueOf(false);
    this.showtitle = Boolean.valueOf(newshowTitle);
    this.skin = newskin;
  }

  public int getDamage() {
    return this.damage;
  }

  public void setDamage(int i) {
    this.damage = i;
  }

  public boolean getShowHp()
  {
    return this.showHp;
  }

  public void setShowHp(boolean i) {
    this.showHp = i;
  }

  public int getMaxHealth()
  {
    return this.MaxHealth;
  }

  public void setMaxHealth(int i) {
    this.MaxHealth = i;
  }

  public int getHealth()
  {
    return this.Health;
  }

  public void sethealth(int i) {
    this.Health = i;
  }

  public void addPercent(int i)
  {
    this.percent.add(Integer.valueOf(i));
  }

  public boolean hasPercent(int i) {
    if (this.percent.contains(Integer.valueOf(i)))
    {
      return true;
    }
    return false;
  }

  public String getName()
  {
    return this.name;
  }

  public void setName(String i) {
    this.name = i;
  }

  public boolean getNatural()
  {
    return this.naturalSpawned.booleanValue();
  }

  public void setNatural(Boolean i) {
    this.naturalSpawned = i;
  }

  public LivingEntity getLivingEntity()
  {
    return this.entity;
  }

  public void setEntity(LivingEntity i) {
    this.entity = i;
  }

  public int getId()
  {
    return this.entity.getEntityId();
  }

  public Location getLocation() {
    return this.entity.getLocation();
  }

  public Location getWorkingLocation()
  {
    if (this.saved)
      return this.savedlocation;
    return this.entity.getLocation();
  }

  public Location getSpawnLocation()
  {
    return this.spawnlocation;
  }

  public void setSavedLocation(Location l)
  {
    this.savedlocation = l;
  }

  public Location getSavedLocation() {
    return this.savedlocation;
  }

  public void setSaved(boolean i) {
    this.saved = i;
  }

  public boolean getSaved() {
    return this.saved;
  }

  public List<String> getItems() {
    return this.Items;
  }

  public List<String> getSkill() {
    return this.Skills;
  }

  public void setRemoveSkill(int i) {
    this.Skills.set(i, "null");
  }

  public String getEntitySpawnName() {
    return this.entityspawnname;
  }

  public void setTimer(String name) {
    this.Timer = name;
  }

  public String getTimer() {
    return this.Timer;
  }

  public List<String> getEffects() {
    return this.Effects;
  }

  public String getSkinUrl() {
    return this.skin;
  }

  public Boss setSkinUrl(String skin) {
    this.skin = skin;
    return this;
  }

  public boolean isTitleShowed() {
    return this.showtitle.booleanValue();
  }

  public Boss showTitle(Boolean show) {
    this.showtitle = show;
    return this;
  }

  private List<String> AddSkills()
  {
    List skills = new ArrayList();
    for (String s : getSkill())
    {
      String[] Parts = s.split(" ");
      if (Parts[0].equalsIgnoreCase("effect"))
      {
        skills.add(Parts[1]);
      }
    }
    return skills;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Boss.Boss
 * JD-Core Version:    0.6.2
 */