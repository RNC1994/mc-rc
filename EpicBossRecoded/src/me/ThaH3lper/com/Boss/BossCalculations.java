package me.ThaH3lper.com.Boss;

import me.ThaH3lper.com.EpicBoss;
import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;

public class BossCalculations
{
  private EpicBoss eb;

  public BossCalculations(EpicBoss boss)
  {
    this.eb = boss;
  }

  public boolean isBoss(Entity e) {
    if (this.eb.BossList != null)
    {
      for (Boss boss : this.eb.BossList)
      {
        if (boss.getLivingEntity() != null)
        {
          if (e.getEntityId() == boss.getId())
          {
            return true;
          }
        }
      }
    }
    return false;
  }

  public boolean isBossLiv(LivingEntity e) {
    if (this.eb.BossList != null)
    {
      for (Boss boss : this.eb.BossList)
      {
        if (boss.getLivingEntity() != null)
        {
          if (e.getEntityId() == boss.getId())
          {
            return true;
          }
        }
      }
    }
    return false;
  }

  public Boss getBoss(Entity e) {
    int id = e.getEntityId();
    if (this.eb.BossList != null)
    {
      for (Boss boss : this.eb.BossList)
      {
        if (!boss.getSaved())
        {
          if (id == boss.getId())
          {
            return boss;
          }
        }
      }
    }
    return null;
  }

  public Boolean BossHited(LivingEntity e) {
    if (this.eb.HeroesEnabled)
    {
      return Boolean.valueOf(false);
    }
    if (e.getHealth() == e.getMaxHealth())
    {
      return Boolean.valueOf(true);
    }
    return Boolean.valueOf(false);
  }

  public Boolean BossExist(Boss b) {
    if (b.getLivingEntity() != null)
    {
      for (World w : Bukkit.getWorlds())
      {
        try
        {
          for (Entity e : w.getEntities())
          {
            if (b.getId() == e.getEntityId())
            {
              return Boolean.valueOf(true);
            }
          }
        }
        catch (Exception e) {
          return null;
        }
      }
    }
    return Boolean.valueOf(false);
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Boss.BossCalculations
 * JD-Core Version:    0.6.2
 */