package me.ThaH3lper.com.Chunk;

import java.util.ArrayList;
import java.util.List;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkUnloadEvent;

public class ChunkUnload
  implements Listener
{
  private EpicBoss eb;

  public ChunkUnload(EpicBoss eb)
  {
    this.eb = eb;
  }

  @EventHandler(priority=EventPriority.HIGH)
  public void Chunk(ChunkUnloadEvent e)
  {
    for (Boss b : getChunkBosses(e.getChunk()))
    {
      b.getLivingEntity().remove();
    }
  }

  public List<Boss> getChunkBosses(Chunk c)
  {
    List bosses = new ArrayList();
    for (Boss b : this.eb.BossList)
    {
      if (!b.getSaved())
      {
        if (b.getLocation().getChunk() == c)
        {
          bosses.add(b);
        }
      }
    }
    return bosses;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Chunk.ChunkUnload
 * JD-Core Version:    0.6.2
 */