package me.ThaH3lper.com;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.FileConfigurationOptions;
import org.bukkit.configuration.file.YamlConfiguration;

public class SaveLoad
{
  private FileConfiguration DataConfig = null;
  private File data = null;
  private EpicBoss eb;
  private String file;
  private File thefile;

  public SaveLoad(EpicBoss boss, String newfile)
  {
    this.eb = boss;
    this.file = newfile;
    this.thefile = new File(this.eb.getDataFolder(), newfile);
    if (this.thefile.exists())
    {
      this.data = this.thefile;
    }
    reloadCustomConfig();
    saveCustomConfig();
  }
  public void reloadCustomConfig() {
    if (this.data == null)
    {
      this.data = new File(this.eb.getDataFolder(), this.file);
      this.DataConfig = YamlConfiguration.loadConfiguration(this.data);
      InputStream defConfigStream = this.eb.getResource(this.file);
      if (defConfigStream != null)
      {
        YamlConfiguration defConfig = YamlConfiguration.loadConfiguration(defConfigStream);
        this.DataConfig.setDefaults(defConfig);
      }
      getCustomConfig().options().copyDefaults(true);
      this.eb.logger.info(this.file + " did not exist! Generated a new one!");
    }
    else
    {
      this.DataConfig = YamlConfiguration.loadConfiguration(this.data);
    }
  }

  public FileConfiguration getCustomConfig() {
    if (this.DataConfig == null) {
      reloadCustomConfig();
    }
    return this.DataConfig;
  }

  public void saveCustomConfig() {
    if ((this.DataConfig == null) || (this.data == null))
      return;
    try
    {
      getCustomConfig().save(this.data);
    } catch (IOException ex) {
      this.eb.getLogger().log(Level.SEVERE, "Could not save config to " + this.data, ex);
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.SaveLoad
 * JD-Core Version:    0.6.2
 */