package me.ThaH3lper.com.Timer;

import me.ThaH3lper.com.EpicBoss;

public class Timer
{
  private int time;
  private int maxtime;
  private String name;
  private String location;
  private String boss;
  private String spawnmsg;
  private EpicBoss eb;

  public Timer(String newname, String newboss, String newlocation, int newtime, EpicBoss neweb)
  {
    this.maxtime = newtime;
    this.boss = newboss;
    this.time = 0;
    this.spawnmsg = "";
    this.name = newname;
    this.location = newlocation;
    this.eb = neweb;
  }

  public String getName() {
    return this.name;
  }

  public int getMaxTime() {
    return this.maxtime;
  }

  public int getTime() {
    return this.time;
  }

  public void setTime(int ntime) {
    this.time = ntime;
  }

  public void setText(String newmsg) {
    this.spawnmsg = newmsg;
  }

  public String getText() {
    return this.spawnmsg;
  }

  public String getBossName() {
    return this.boss;
  }

  public String getLocationStr() {
    return this.location;
  }

  public void lower() {
    if ((this.time <= 0) && (this.time >= -9))
    {
      this.eb.timerstuff.spawndeath(this);
      this.time = -10;
    }
    else if (this.time != -10)
    {
      this.time -= 5;
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Timer.Timer
 * JD-Core Version:    0.6.2
 */