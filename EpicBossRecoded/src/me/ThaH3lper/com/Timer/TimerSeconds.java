package me.ThaH3lper.com.Timer;

import java.util.Iterator;
import java.util.List;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.Timer.Spawn.Despawn;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.scheduler.BukkitScheduler;

public class TimerSeconds
{
  public Despawn despawn;
  private EpicBoss eb;

  public TimerSeconds(EpicBoss boss)
  {
    this.eb = boss;
    this.despawn = new Despawn(this.eb);
    this.eb.getServer().getScheduler().scheduleSyncRepeatingTask(this.eb, new Runnable()
    {
      public void run() {
        TimerSeconds.this.despawn.DeSpawnEvent(TimerSeconds.this.eb);
        TimerSeconds.this.eb.timerstuff.lower();
        TimerSeconds.this.skillsShow();
      }
    }
    , 100L, 100L);
  }

  public void skillsShow()
  {
    Iterator localIterator2;
    label133: for (Iterator localIterator1 = this.eb.BossList.iterator(); localIterator1.hasNext(); 
      localIterator2.hasNext())
    {
      Boss b = (Boss)localIterator1.next();

      if ((b.getEffects() == null) || (b.getSaved()))
        break label133;
      localIterator2 = b.getEffects().iterator(); continue; String s = (String)localIterator2.next();

      if (s.equalsIgnoreCase("fire"))
      {
        b.getWorkingLocation().getWorld().playEffect(b.getWorkingLocation(), Effect.MOBSPAWNER_FLAMES, 0);
      }
      if (s.equalsIgnoreCase("ender"))
      {
        b.getWorkingLocation().getWorld().playEffect(b.getWorkingLocation(), Effect.ENDER_SIGNAL, 0);
      }
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Timer.TimerSeconds
 * JD-Core Version:    0.6.2
 */