package me.ThaH3lper.com.Timer;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.LoadBosses.LoadBoss;
import me.ThaH3lper.com.LoadBosses.LoadConfigs;
import me.ThaH3lper.com.SaveLoad;
import me.ThaH3lper.com.Timer.Spawn.Despawn;
import me.ThaH3lper.com.locations.LocationStuff;
import me.ThaH3lper.com.locations.Locations;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;

public class TimerStuff
{
  EpicBoss eb;

  public TimerStuff(EpicBoss neweb)
  {
    this.eb = neweb;
    LoadAllTimers();
  }

  public void addTimer(String name, String bossname, String location, int time)
  {
    this.eb.TimersList.add(new Timer(name, bossname, location, time, this.eb));
    saveAllTimers();
  }

  public void removeTimer(String name)
  {
    if (this.eb.TimersList != null)
    {
      int i = 0;
      while (i < this.eb.TimersList.size())
      {
        if (((Timer)this.eb.TimersList.get(i)).getName().equals(name))
        {
          this.eb.TimersList.remove(i);
          saveAllTimers();
        }
        i++;
      }
    }
  }

  public void saveAllTimers()
  {
    if (this.eb.TimersList != null)
    {
      List saved = new ArrayList();
      for (Timer time : this.eb.TimersList)
      {
        String save = time.getName() + ":" + time.getBossName() + ":" + time.getLocationStr() + ":" + time.getMaxTime();
        if (time.getText() != "")
        {
          save = time.getName() + ":" + time.getBossName() + ":" + time.getLocationStr() + ":" + time.getMaxTime() + ":" + time.getText();
        }
        saved.add(save);
      }
      this.eb.SavedData.reloadCustomConfig();
      this.eb.SavedData.getCustomConfig().set("Timers", saved);
      this.eb.SavedData.saveCustomConfig();
    }
  }

  public void LoadAllTimers() {
    if (this.eb.SavedData.getCustomConfig().contains("Timers"))
    {
      if (this.eb.SavedData.getCustomConfig().getStringList("Timers") != null)
      {
        for (String s : this.eb.SavedData.getCustomConfig().getStringList("Timers"))
        {
          String[] Splits = s.split(":");
          if (this.eb.loadconfig.getLoadBoss(Splits[1]) != null)
          {
            if (this.eb.locationstuff.locationExict(Splits[2]))
            {
              Timer time = new Timer(Splits[0], Splits[1], Splits[2], Integer.parseInt(Splits[3]), this.eb);
              this.eb.TimersList.add(time);
              if (Splits.length == 5)
              {
                time.setText(Splits[4]);
              }
              saveAllTimers();
            }
            else
            {
              this.eb.logger.warning("Timer: " + Splits[0] + " could not be loaded since location " + Splits[2] + " dose not exict!");
            }
          }
          else
          {
            this.eb.logger.warning("Timer: " + Splits[0] + " could not be loaded since boss " + Splits[1] + " dose not exict!");
          }
        }
      }
    }
  }

  public void lower() {
    if (this.eb.TimersList != null)
    {
      for (Timer time : this.eb.TimersList)
      {
        time.lower();
      }
    }
  }

  public void Death(Boss b) {
    if (getTimer(b.getTimer()) != null)
    {
      Timer time = getTimer(b.getTimer());
      time.setTime(time.getMaxTime());
    }
  }

  public void spawndeath(Timer time) {
    LoadBoss lb = this.eb.loadconfig.getLoadBoss(time.getBossName());
    Locations loc = this.eb.locationstuff.getLocations(time.getLocationStr());
    if ((lb != null) && (loc != null))
    {
      Boss b = new Boss(lb.getName(), lb.getHealth(), loc.getLocation(), lb.getType(), lb.getDamage(), lb.getShowhp(), lb.getItems(), lb.getSkills(), lb.getShowtitle(), lb.getSkin());
      b.setTimer(time.getName());
      this.eb.BossList.add(b);

      this.eb.timer.despawn.DeSpawnEvent(this.eb);
      if (!time.getText().equals(""))
      {
        String s = time.getText();
        s = s.replace("_", " ");
        s = ChatColor.translateAlternateColorCodes('&', s);
        Bukkit.broadcastMessage(s);
      }
    }
  }

  public Timer getTimer(String name) {
    if (this.eb.TimersList != null)
    {
      for (Timer time : this.eb.TimersList)
      {
        if (time.getName().equals(name))
        {
          return time;
        }
      }
    }
    return null;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Timer.TimerStuff
 * JD-Core Version:    0.6.2
 */