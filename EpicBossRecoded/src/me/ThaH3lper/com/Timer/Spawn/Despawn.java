package me.ThaH3lper.com.Timer.Spawn;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.CharacterManager;
import com.herocraftonline.heroes.characters.Monster;
import java.util.ArrayList;
import java.util.List;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.Boss.BossCalculations;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.LoadBosses.LoadBossEquip;
import me.ThaH3lper.com.Mobs;
import me.ThaH3lper.com.Skills.SkillsHandler;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.getspout.spoutapi.Spout;
import org.getspout.spoutapi.SpoutServer;
import org.getspout.spoutapi.player.EntitySkinType;

public class Despawn
{
  private EpicBoss eb;

  public Despawn(EpicBoss eb)
  {
    this.eb = eb;
  }

  public void DeSpawnEvent(EpicBoss eb)
  {
    List remove = new ArrayList();
    if (eb.BossList != null)
    {
      for (Boss boss : eb.BossList)
      {
        if ((!boss.getSaved()) && (!boss.getNatural()))
        {
          if (boss.getLocation() != null)
          {
            if (!PlayersInside(boss.getLocation()))
            {
              if (!boss.getEntitySpawnName().equals("enderdragon"))
              {
                boss.setSaved(true);
                boss.setSavedLocation(boss.getLocation());
                boss.getLivingEntity().remove();
                if (eb.regain)
                {
                  boss.sethealth(boss.getMaxHealth());
                }
              }
            }
          }
        }
        else if (boss.getSaved())
        {
          if ((PlayersInside(boss.getSavedLocation())) || (boss.getNatural()))
          {
            boss.setSaved(false);
            LivingEntity l = (LivingEntity)eb.mobs.SpawnMob(boss.getEntitySpawnName(), boss.getSavedLocation());
            l.setHealth(l.getMaxHealth() - 1);
            boss.setEntity(l);
            eb.loadbossequip.SetEqupiment(boss);
            eb.skillhandler.skills(boss, null);

            if (eb.HeroesEnabled)
            {
              eb.heroes.getCharacterManager().getMonster(l).setDamage(boss.getDamage());
              eb.heroes.getCharacterManager().getMonster(l).setMaxHealth(999999999);
            }

            if ((eb.SpoutEnabled) && (boss.getSkinUrl() != null))
            {
              Spout.getServer().setEntitySkin(l, boss.getSkinUrl(), EntitySkinType.DEFAULT);
              Bukkit.broadcastMessage("!");
            }
            if ((eb.SpoutEnabled) && (boss.isTitleShowed()))
            {
              String s = boss.getName().replace("_", " ");
              Spout.getServer().setTitle(l, s);
            }

          }

        }
        else if (!eb.bossCalculator.BossExist(boss).booleanValue())
        {
          int i = 0;
          while (i < eb.BossList.size())
          {
            if (((Boss)eb.BossList.get(i)).equals(boss))
            {
              remove.add((Boss)eb.BossList.get(i));
            }
            i++;
          }
          for (Boss b : remove)
          {
            if (b.getLivingEntity() != null) {
              b.getLivingEntity().remove();
            }
          }
        }

        if ((!boss.getSaved()) && (!boss.getNatural()))
        {
          if (PlayersInside(boss.getSavedLocation()))
          {
            if (eb.bossCalculator.BossExist(boss) != null)
            {
              if (!eb.bossCalculator.BossExist(boss).booleanValue())
              {
                boss.setSaved(true);
                boss.setSavedLocation(boss.getLocation());
              }
            }
          }
        }
      }
      eb.BossList.removeAll(remove);
    }
  }

  public void DeSpawnBoss(Boss b)
  {
    if (b.getNatural())
    {
      b.setSaved(false);
      LivingEntity l = (LivingEntity)this.eb.mobs.SpawnMob(b.getEntitySpawnName(), b.getSavedLocation());
      l.setHealth(l.getMaxHealth() - 1);

      b.setEntity(l);
      this.eb.loadbossequip.SetEqupiment(b);
      this.eb.skillhandler.skills(b, null);

      if (this.eb.HeroesEnabled)
      {
        this.eb.heroes.getCharacterManager().getMonster(l).setDamage(b.getDamage());
        this.eb.heroes.getCharacterManager().getMonster(l).setMaxHealth(999999999);
      }

      if ((this.eb.SpoutEnabled) && (b.getSkinUrl() != null))
      {
        Spout.getServer().setEntitySkin(l, b.getSkinUrl(), EntitySkinType.DEFAULT);
        Bukkit.broadcastMessage("!");
      }
      if ((this.eb.SpoutEnabled) && (b.isTitleShowed()))
      {
        String s = b.getName().replace("_", " ");
        Spout.getServer().setTitle(l, s);
      }
    }
  }

  public boolean PlayersInside(Location l)
  {
    for (Player p : Bukkit.getServer().getOnlinePlayers())
    {
      if (l.getWorld() == p.getWorld())
      {
        if (l.distance(p.getLocation()) < 40.0D)
        {
          return true;
        }
      }
    }
    return false;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Timer.Spawn.Despawn
 * JD-Core Version:    0.6.2
 */