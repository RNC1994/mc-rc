package me.ThaH3lper.com.Commands;

import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.LoadBosses.LoadConfigs;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandsPlayer
{
  private EpicBoss eb;
  String s = ChatColor.DARK_RED + "-------------------" + ChatColor.GRAY + "[ " + ChatColor.RED + ChatColor.BOLD + "EpicBoss" + ChatColor.GRAY + " ]" + ChatColor.DARK_RED + "-------------------";
  private Bosses bosses;
  private Timers timer;
  private Location location;
  private Bossegg bossegg;

  public CommandsPlayer(EpicBoss boss)
  {
    this.eb = boss;
    this.bosses = new Bosses(this.eb);
    this.timer = new Timers(this.eb);
    this.location = new Location(this.eb);
    this.bossegg = new Bossegg(this.eb);
  }

  public void Command(CommandSender sender, Command cmd, String commandlabel, String[] args) {
    Player p = (Player)sender;
    if (args.length == 0)
    {
      p.sendMessage(this.s);
      p.sendMessage(ChatColor.RED + "/eb boss" + ChatColor.GRAY + ChatColor.ITALIC + "   Commands/info about Bosses");
      p.sendMessage(ChatColor.RED + "/eb location" + ChatColor.GRAY + ChatColor.ITALIC + "  Commands/info about Locations");
      p.sendMessage(ChatColor.RED + "/eb timers" + ChatColor.GRAY + ChatColor.ITALIC + "  Commands/info about Timers");
      p.sendMessage(ChatColor.RED + "/eb bossegg" + ChatColor.GRAY + ChatColor.ITALIC + "  Commands/info about bossegg");
      p.sendMessage(ChatColor.RED + "/eb reload" + ChatColor.GRAY + ChatColor.ITALIC + "    Reload changes in Bosses.yml");
      p.sendMessage(ChatColor.DARK_GRAY + ChatColor.ITALIC + "(Version 1.3.2, Coded by ThaH3lper)");
    }
    if (args.length >= 1)
    {
      if (args[0].equals("reload"))
      {
        if (p.hasPermission("epicboss.reload"))
        {
          this.eb.loadconfig.LoadBosses();
          p.sendMessage(ChatColor.GREEN + "EpicBoss reloded!");
        }
      }
      else if (args[0].equals("boss"))
      {
        if (p.hasPermission("epicboss.boss"))
        {
          this.bosses.Command(p, cmd, commandlabel, args);
        }
      }
      else if (args[0].equals("bossegg"))
      {
        if (p.hasPermission("epicboss.bossegg"))
        {
          this.bossegg.Command(p, cmd, commandlabel, args);
        }
      }
      else if (args[0].equals("timers"))
      {
        if (p.hasPermission("epicboss.timers"))
        {
          this.timer.Command(p, cmd, commandlabel, args);
        }
      }
      else if (args[0].equals("location"))
      {
        if (p.hasPermission("epicboss.location"))
        {
          this.location.Command(p, cmd, commandlabel, args);
        }
      }
      else
      {
        p.sendMessage(this.s);
        p.sendMessage(ChatColor.RED + "/eb boss" + ChatColor.GRAY + ChatColor.ITALIC + "   Commands/info about Bosses");
        p.sendMessage(ChatColor.RED + "/eb location" + ChatColor.GRAY + ChatColor.ITALIC + "  Commands/info about Locations");
        p.sendMessage(ChatColor.RED + "/eb timers" + ChatColor.GRAY + ChatColor.ITALIC + "  Commands/info about Timers");
        p.sendMessage(ChatColor.RED + "/eb bossegg" + ChatColor.GRAY + ChatColor.ITALIC + "  Commands/info about bossegg");
        p.sendMessage(ChatColor.RED + "/eb reload" + ChatColor.GRAY + ChatColor.ITALIC + "    Reload changes in Bosses.yml");
      }
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Commands.CommandsPlayer
 * JD-Core Version:    0.6.2
 */