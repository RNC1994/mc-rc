package me.ThaH3lper.com.Commands;

import me.ThaH3lper.com.EpicBoss;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandsHandler
  implements CommandExecutor
{
  private CommandsConsole CC;
  private CommandsPlayer CP;
  private EpicBoss eb;

  public CommandsHandler(EpicBoss boss)
  {
    this.eb = boss;
    this.CP = new CommandsPlayer(this.eb);
    this.CC = new CommandsConsole(this.eb);
  }

  public boolean onCommand(CommandSender sender, Command cmd, String commandlabel, String[] args) {
    if ((sender instanceof Player))
    {
      this.CP.Command(sender, cmd, commandlabel, args);
    }
    else
    {
      this.CC.Command(sender, cmd, commandlabel, args);
    }
    return false;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Commands.CommandsHandler
 * JD-Core Version:    0.6.2
 */