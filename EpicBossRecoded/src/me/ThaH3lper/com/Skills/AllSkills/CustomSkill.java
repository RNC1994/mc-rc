package me.ThaH3lper.com.Skills.AllSkills;

import java.util.Random;
import me.ThaH3lper.com.Api.BossSkillEvent;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.plugin.PluginManager;

public class CustomSkill
{
  private EpicBoss eb;
  Random r = new Random();
  private BossSkillEvent event;

  public CustomSkill(EpicBoss boss)
  {
    this.eb = boss;
  }

  public void executeThrow(String s, Boss b, int index) {
    String[] parts = s.split(" ");
    float chance = Float.parseFloat(parts[2]);
    if (parts[1].contains(">"))
    {
      String exe = parts[1].replace(">", "");
      if (b.getHealth() > Integer.parseInt(exe))
      {
        GoCustom(chance, b, parts[0]);
      }
    }
    else if (parts[1].contains("="))
    {
      String exe = parts[1].replace("=", "");
      if (b.getHealth() <= Integer.parseInt(exe))
      {
        GoCustom(chance, b, parts[0]);
        b.setRemoveSkill(index);
      }
    }
    else if (parts[1].contains("<"))
    {
      String exe = parts[1].replace("<", "");
      if (b.getHealth() < Integer.parseInt(exe))
      {
        GoCustom(chance, b, parts[0]);
      }
    }
    else if (parts[1].contains("/"))
    {
      String exe = parts[1].replace("/", "");
      String[] value = exe.split("-");
      if ((b.getHealth() < Integer.parseInt(value[0])) && (b.getHealth() > Integer.parseInt(value[1])))
      {
        GoCustom(chance, b, parts[0]);
      }
    }
  }

  public void GoCustom(float chance, Boss b, String name) {
    if (this.r.nextFloat() <= chance)
    {
      this.event = new BossSkillEvent(this.eb, b, name, Boolean.valueOf(true));
      Bukkit.getServer().getPluginManager().callEvent(this.event);
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Skills.AllSkills.CustomSkill
 * JD-Core Version:    0.6.2
 */