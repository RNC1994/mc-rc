package me.ThaH3lper.com.Skills.AllSkills;

import java.util.Random;
import me.ThaH3lper.com.Api.BossSkillEvent;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.Skills.SkillsHandler;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Potions
{
  private EpicBoss eb;
  Random r = new Random();

  public Potions(EpicBoss boss)
  {
    this.eb = boss;
  }

  public void executePotions(String s, Boss b, int index) {
    String[] parts = s.split(" ");
    String[] settings = parts[1].split(":");
    float chance = Float.parseFloat(parts[3]);
    if (parts[2].contains(">"))
    {
      String exe = parts[2].replace(">", "");
      if (b.getHealth() > Integer.parseInt(exe))
      {
        SetPlayer(Integer.parseInt(settings[0]), settings[1], Integer.parseInt(settings[3]), Integer.parseInt(settings[2]), chance, b);
      }
    }
    else if (parts[2].contains("="))
    {
      String exe = parts[2].replace("=", "");
      if (b.getHealth() <= Integer.parseInt(exe))
      {
        SetPlayer(Integer.parseInt(settings[0]), settings[1], Integer.parseInt(settings[3]), Integer.parseInt(settings[2]), chance, b);
        b.setRemoveSkill(index);
      }
    }
    else if (parts[2].contains("<"))
    {
      String exe = parts[2].replace("<", "");
      if (b.getHealth() < Integer.parseInt(exe))
      {
        SetPlayer(Integer.parseInt(settings[0]), settings[1], Integer.parseInt(settings[3]), Integer.parseInt(settings[2]), chance, b);
      }
    }
    else if (parts[2].contains("/"))
    {
      String exe = parts[2].replace("/", "");
      String[] value = exe.split("-");
      if ((b.getHealth() < Integer.parseInt(value[0])) && (b.getHealth() > Integer.parseInt(value[1])))
      {
        SetPlayer(Integer.parseInt(settings[0]), settings[1], Integer.parseInt(settings[3]), Integer.parseInt(settings[2]), chance, b);
      }
    }
  }

  public void SetPlayer(int radious, String potion, int lvl, int duration, float chance, Boss b) {
    if (this.r.nextFloat() <= chance)
    {
      this.eb.skillhandler.event = new BossSkillEvent(this.eb, b, "potion", Boolean.valueOf(false));
      Bukkit.getServer().getPluginManager().callEvent(this.eb.skillhandler.event);
      if (this.eb.skillhandler.getPlayersRadious(radious, b) != null)
      {
        for (Player p : this.eb.skillhandler.getPlayersRadious(radious, b))
        {
          PotionEffect pe = new PotionEffect(PotionEffectType.getByName(potion), duration * 20, lvl - 1);
          if (pe != null)
          {
            p.addPotionEffect(pe);
          }
        }
      }
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Skills.AllSkills.Potions
 * JD-Core Version:    0.6.2
 */