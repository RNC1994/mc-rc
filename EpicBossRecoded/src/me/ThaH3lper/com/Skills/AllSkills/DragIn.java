package me.ThaH3lper.com.Skills.AllSkills;

import java.util.Random;
import me.ThaH3lper.com.Api.BossSkillEvent;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.Skills.SkillsHandler;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;

public class DragIn
{
  private EpicBoss eb;
  Random r = new Random();

  public DragIn(EpicBoss boss)
  {
    this.eb = boss;
  }

  public void executeDragin(String s, Boss b, int index) {
    String[] parts = s.split(" ");
    String[] settings = parts[1].split(":");
    float chance = Float.parseFloat(parts[3]);
    if (parts[2].contains(">"))
    {
      String exe = parts[2].replace(">", "");
      if (b.getHealth() > Integer.parseInt(exe))
      {
        DragPlayer(Integer.parseInt(settings[0]), chance, b);
      }
    }
    else if (parts[2].contains("="))
    {
      String exe = parts[2].replace("=", "");
      if (b.getHealth() <= Integer.parseInt(exe))
      {
        DragPlayer(Integer.parseInt(settings[0]), chance, b);
        b.setRemoveSkill(index);
      }
    }
    else if (parts[2].contains("<"))
    {
      String exe = parts[2].replace("<", "");
      if (b.getHealth() < Integer.parseInt(exe))
      {
        DragPlayer(Integer.parseInt(settings[0]), chance, b);
      }
    }
    else if (parts[2].contains("/"))
    {
      String exe = parts[2].replace("/", "");
      String[] value = exe.split("-");
      if ((b.getHealth() < Integer.parseInt(value[0])) && (b.getHealth() > Integer.parseInt(value[1])))
      {
        DragPlayer(Integer.parseInt(settings[0]), chance, b);
      }
    }
  }

  public void DragPlayer(int radious, float chance, Boss b) {
    if (this.r.nextFloat() <= chance)
    {
      this.eb.skillhandler.event = new BossSkillEvent(this.eb, b, "Dragin", Boolean.valueOf(false));
      Bukkit.getServer().getPluginManager().callEvent(this.eb.skillhandler.event);
      if (this.eb.skillhandler.getPlayersRadious(radious, b) != null)
      {
        for (Player p : this.eb.skillhandler.getPlayersRadious(radious, b))
        {
          p.teleport(b.getLocation());
        }
      }
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Skills.AllSkills.DragIn
 * JD-Core Version:    0.6.2
 */