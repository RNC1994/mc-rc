package me.ThaH3lper.com.Skills.AllSkills;

import java.util.List;
import java.util.Random;
import me.ThaH3lper.com.Api.BossSkillEvent;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.LoadBosses.LoadBoss;
import me.ThaH3lper.com.LoadBosses.LoadConfigs;
import me.ThaH3lper.com.Mobs;
import me.ThaH3lper.com.Skills.SkillsHandler;
import me.ThaH3lper.com.Timer.Spawn.Despawn;
import me.ThaH3lper.com.Timer.TimerSeconds;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.plugin.PluginManager;

public class Swarm
{
  private EpicBoss eb;
  Random r = new Random();

  public Swarm(EpicBoss boss)
  {
    this.eb = boss;
  }

  public void executeSwarm(String s, Boss b, int index) {
    String[] parts = s.split(" ");
    String[] settings = parts[1].split(":");
    int amount = Integer.parseInt(settings[1]);
    float chance = Float.parseFloat(parts[3]);
    if (parts[2].contains(">"))
    {
      String exe = parts[2].replace(">", "");
      if (b.getHealth() > Integer.parseInt(exe))
      {
        spawn(settings[0], amount, b, chance);
      }
    }
    else if (parts[2].contains("="))
    {
      String exe = parts[2].replace("=", "");
      if (b.getHealth() <= Integer.parseInt(exe))
      {
        spawn(settings[0], amount, b, chance);
        b.setRemoveSkill(index);
      }
    }
    else if (parts[2].contains("<"))
    {
      String exe = parts[2].replace("<", "");
      if (b.getHealth() < Integer.parseInt(exe))
      {
        spawn(settings[0], amount, b, chance);
      }
    }
    else if (parts[2].contains("/"))
    {
      String exe = parts[2].replace("/", "");
      String[] value = exe.split("-");
      if ((b.getHealth() < Integer.parseInt(value[0])) && (b.getHealth() > Integer.parseInt(value[1])))
      {
        spawn(settings[0], amount, b, chance);
      }
    }
  }

  public void spawn(String mob, int amount, Boss b, float Chance)
  {
    if (this.r.nextFloat() <= Chance)
    {
      this.eb.skillhandler.event = new BossSkillEvent(this.eb, b, "swarm", Boolean.valueOf(false));
      Bukkit.getServer().getPluginManager().callEvent(this.eb.skillhandler.event);
      if (mob.contains("$"))
      {
        String bossname = mob.replace("$", "");
        LoadBoss lb = this.eb.loadconfig.getLoadBoss(bossname);
        if (lb != null)
        {
          int i = 1;
          while (i <= amount)
          {
            i++;
            this.eb.BossList.add(new Boss(lb.getName(), lb.getHealth(), b.getLocation(), lb.getType(), lb.getDamage(), lb.getShowhp(), lb.getItems(), lb.getSkills(), lb.getShowtitle(), lb.getSkin()));
          }
        }
      }
      else
      {
        int i = 1;
        while (i <= amount)
        {
          i++;
          this.eb.mobs.SpawnMob(mob, b.getLocation());
        }
      }
      this.eb.timer.despawn.DeSpawnEvent(this.eb);
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Skills.AllSkills.Swarm
 * JD-Core Version:    0.6.2
 */