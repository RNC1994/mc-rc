package me.ThaH3lper.com.Skills.AllSkills;

import java.util.Random;
import me.ThaH3lper.com.Api.BossSkillEvent;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.Skills.SkillsHandler;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;

public class Lightning
{
  private EpicBoss eb;
  Random r = new Random();

  public Lightning(EpicBoss boss)
  {
    this.eb = boss;
  }

  public void executeLightning(String s, Boss b, int index) {
    String[] parts = s.split(" ");
    String[] settings = parts[1].split(":");
    float chance = Float.parseFloat(parts[3]);
    if (parts[2].contains(">"))
    {
      String exe = parts[2].replace(">", "");
      if (b.getHealth() > Integer.parseInt(exe))
      {
        lightPlayer(Integer.parseInt(settings[0]), Integer.parseInt(settings[1]), b, chance);
      }
    }
    else if (parts[2].contains("="))
    {
      String exe = parts[2].replace("=", "");
      if (b.getHealth() <= Integer.parseInt(exe))
      {
        lightPlayer(Integer.parseInt(settings[0]), Integer.parseInt(settings[1]), b, chance);
        b.setRemoveSkill(index);
      }
    }
    else if (parts[2].contains("<"))
    {
      String exe = parts[2].replace("<", "");
      if (b.getHealth() < Integer.parseInt(exe))
      {
        lightPlayer(Integer.parseInt(settings[0]), Integer.parseInt(settings[1]), b, chance);
      }
    }
    else if (parts[2].contains("/"))
    {
      String exe = parts[2].replace("/", "");
      String[] value = exe.split("-");
      if ((b.getHealth() < Integer.parseInt(value[0])) && (b.getHealth() > Integer.parseInt(value[1])))
      {
        lightPlayer(Integer.parseInt(settings[0]), Integer.parseInt(settings[1]), b, chance);
      }
    }
  }

  public void lightPlayer(int radious, int damage, Boss b, float chance) {
    if (this.r.nextFloat() <= chance)
    {
      this.eb.skillhandler.event = new BossSkillEvent(this.eb, b, "lightning", Boolean.valueOf(false));
      Bukkit.getServer().getPluginManager().callEvent(this.eb.skillhandler.event);
      if (this.eb.skillhandler.getPlayersRadious(radious, b) != null)
      {
        for (Player p : this.eb.skillhandler.getPlayersRadious(radious, b))
        {
          p.getLocation().getWorld().strikeLightningEffect(p.getLocation());
          p.damage(damage);
        }
      }
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Skills.AllSkills.Lightning
 * JD-Core Version:    0.6.2
 */