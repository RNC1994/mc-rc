package me.ThaH3lper.com.Skills.AllSkills;

import java.util.Random;
import me.ThaH3lper.com.Api.BossSkillEvent;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.Skills.SkillsHandler;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;

public class Teleport
{
  private EpicBoss eb;
  Random r = new Random();

  public Teleport(EpicBoss boss)
  {
    this.eb = boss;
  }

  public void executeTeleport(String s, Boss b, int index, Player p) {
    String[] parts = s.split(" ");
    String[] settings = parts[1].split(":");
    float chance = Float.parseFloat(parts[3]);
    if (parts[2].contains(">"))
    {
      String exe = parts[2].replace(">", "");
      if (b.getHealth() > Integer.parseInt(exe))
      {
        doTeleport(settings[0], chance, b, p);
      }
    }
    else if (parts[2].contains("="))
    {
      String exe = parts[2].replace("=", "");
      if (b.getHealth() <= Integer.parseInt(exe))
      {
        doTeleport(settings[0], chance, b, p);
        b.setRemoveSkill(index);
      }
    }
    else if (parts[2].contains("<"))
    {
      String exe = parts[2].replace("<", "");
      if (b.getHealth() < Integer.parseInt(exe))
      {
        doTeleport(settings[0], chance, b, p);
      }
    }
    else if (parts[2].contains("/"))
    {
      String exe = parts[2].replace("/", "");
      String[] value = exe.split("-");
      if ((b.getHealth() < Integer.parseInt(value[0])) && (b.getHealth() > Integer.parseInt(value[1])))
      {
        doTeleport(settings[0], chance, b, p);
      }
    }
  }

  public void doTeleport(String s, float chance, Boss b, Player pla) {
    if (this.r.nextFloat() <= chance)
    {
      if (pla != null)
      {
        if (s.equalsIgnoreCase("teleport"))
        {
          b.getLivingEntity().teleport(pla.getLocation());
        }
        else
        {
          Location l = b.getLocation();
          b.getLivingEntity().teleport(pla.getLocation());
          pla.teleport(l);
        }
        this.eb.skillhandler.event = new BossSkillEvent(this.eb, b, "teleport", Boolean.valueOf(false));
        Bukkit.getServer().getPluginManager().callEvent(this.eb.skillhandler.event);
      }
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Skills.AllSkills.Teleport
 * JD-Core Version:    0.6.2
 */