package me.ThaH3lper.com.Skills.AllSkills;

import java.util.Random;
import me.ThaH3lper.com.Api.BossSkillEvent;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.Skills.SkillsHandler;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.Player;
import org.bukkit.plugin.PluginManager;

public class Command
{
  private EpicBoss eb;
  Random r = new Random();

  public Command(EpicBoss boss)
  {
    this.eb = boss;
  }

  public void executeCommand(String s, Boss b, int index, Player p) {
    String[] parts = s.split(" ");
    float chance = Float.parseFloat(parts[3]);
    if (parts[2].contains(">"))
    {
      String exe = parts[2].replace(">", "");
      if (b.getHealth() > Integer.parseInt(exe))
      {
        sendCommand(parts[1], chance, p, b);
      }
    }
    else if (parts[2].contains("="))
    {
      String exe = parts[2].replace("=", "");
      if (b.getHealth() <= Integer.parseInt(exe))
      {
        sendCommand(parts[1], chance, p, b);
        b.setRemoveSkill(index);
      }
    }
    else if (parts[2].contains("<"))
    {
      String exe = parts[2].replace("<", "");
      if (b.getHealth() < Integer.parseInt(exe))
      {
        sendCommand(parts[1], chance, p, b);
      }
    }
    else if (parts[2].contains("/"))
    {
      String exe = parts[2].replace("/", "");
      String[] value = exe.split("-");
      if ((b.getHealth() < Integer.parseInt(value[0])) && (b.getHealth() > Integer.parseInt(value[1])))
      {
        sendCommand(parts[1], chance, p, b);
      }
    }
  }

  public void sendCommand(String s, float chance, Player p, Boss b) {
    if (this.r.nextFloat() <= chance)
    {
      this.eb.skillhandler.event = new BossSkillEvent(this.eb, b, "command", Boolean.valueOf(false));
      Bukkit.getServer().getPluginManager().callEvent(this.eb.skillhandler.event);

      s = s.replace("_", " ");
      s = s.replace("@", "_");
      if (p != null)
        s = s.replace("$player", p.getName());
      s = s.replace("$boss", b.getName());
      Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), s);
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Skills.AllSkills.Command
 * JD-Core Version:    0.6.2
 */