package me.ThaH3lper.com.Skills.AllSkills;

import java.util.Random;
import me.ThaH3lper.com.Api.BossSkillEvent;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.Skills.SkillsHandler;
import org.bukkit.Bukkit;
import org.bukkit.Server;
import org.bukkit.entity.LivingEntity;
import org.bukkit.plugin.PluginManager;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PotionBoss
{
  private EpicBoss eb;
  Random r = new Random();

  public PotionBoss(EpicBoss boss)
  {
    this.eb = boss;
  }

  public void executeBossPotions(String s, Boss b, int index) {
    String[] parts = s.split(" ");
    String[] settings = parts[1].split(":");
    float chance = Float.parseFloat(parts[3]);
    if (parts[2].contains(">"))
    {
      String exe = parts[2].replace(">", "");
      if (b.getHealth() > Integer.parseInt(exe))
      {
        SetBossPotion(settings[0], Integer.parseInt(settings[2]), Integer.parseInt(settings[1]), chance, b);
      }
    }
    else if (parts[2].contains("="))
    {
      String exe = parts[2].replace("=", "");
      if (b.getHealth() <= Integer.parseInt(exe))
      {
        SetBossPotion(settings[0], Integer.parseInt(settings[2]), Integer.parseInt(settings[1]), chance, b);
        b.setRemoveSkill(index);
      }
    }
    else if (parts[2].contains("<"))
    {
      String exe = parts[2].replace("<", "");
      if (b.getHealth() < Integer.parseInt(exe))
      {
        SetBossPotion(settings[0], Integer.parseInt(settings[2]), Integer.parseInt(settings[1]), chance, b);
      }
    }
    else if (parts[2].contains("/"))
    {
      String exe = parts[2].replace("/", "");
      String[] value = exe.split("-");
      if ((b.getHealth() < Integer.parseInt(value[0])) && (b.getHealth() > Integer.parseInt(value[1])))
      {
        SetBossPotion(settings[0], Integer.parseInt(settings[2]), Integer.parseInt(settings[1]), chance, b);
      }
    }
  }

  public void SetBossPotion(String potion, int lvl, int duration, float chance, Boss b) {
    if (this.r.nextFloat() <= chance)
    {
      this.eb.skillhandler.event = new BossSkillEvent(this.eb, b, "potionboss", Boolean.valueOf(false));
      Bukkit.getServer().getPluginManager().callEvent(this.eb.skillhandler.event);
      PotionEffect pe = new PotionEffect(PotionEffectType.getByName(potion), duration * 20, lvl - 1);
      if (pe != null)
      {
        b.getLivingEntity().addPotionEffect(pe);
      }
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Skills.AllSkills.PotionBoss
 * JD-Core Version:    0.6.2
 */