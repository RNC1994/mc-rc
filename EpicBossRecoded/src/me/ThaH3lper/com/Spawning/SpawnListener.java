package me.ThaH3lper.com.Spawning;

import java.util.List;
import java.util.Random;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.LoadBosses.LoadBoss;
import me.ThaH3lper.com.Mobs;
import me.ThaH3lper.com.Timer.Spawn.Despawn;
import me.ThaH3lper.com.Timer.TimerSeconds;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;

public class SpawnListener
  implements Listener
{
  EpicBoss eb;
  Random r = new Random();

  public SpawnListener(EpicBoss eb)
  {
    this.eb = eb;
  }

  @EventHandler
  public void SpawningMob(CreatureSpawnEvent e) {
    if (e.isCancelled())
      return;
    if (e.getSpawnReason() == CreatureSpawnEvent.SpawnReason.NATURAL)
    {
      for (Spawnings sp : this.eb.SpawningsList)
      {
        if (passSpawnReasons(e.getEntity(), sp))
        {
          LoadBoss lb = sp.loadBoss;
          Boss boss = new Boss(lb.getName(), lb.getHealth(), e.getLocation(), lb.getType(), lb.getDamage(), lb.getShowhp(), lb.getItems(), lb.getSkills(), lb.getShowtitle(), lb.getSkin());
          this.eb.BossList.add(boss);
          this.eb.timer.despawn.DeSpawnBoss(boss);
          boss.setNatural(Boolean.valueOf(true));
          e.getEntity().remove();
          return;
        }
      }
    }
  }

  public boolean passSpawnReasons(LivingEntity l, Spawnings sp)
  {
    if (seeType(l, sp))
    {
      if (seeChance(l, sp))
      {
        if (seeWorld(l, sp))
        {
          if (seeBiome(l, sp))
          {
            return true;
          }
        }
      }
    }
    return false;
  }

  public boolean seeType(LivingEntity l, Spawnings sp)
  {
    String[] types = sp.Replace.split(",");
    for (String type : types)
    {
      if (type.equals("all")) {
        return true;
      }
      Entity dummy = this.eb.mobs.SpawnMob(type, l.getLocation());
      if (dummy.getType().equals(l.getType()))
      {
        dummy.remove();
        return true;
      }
      dummy.remove();
    }
    return false;
  }

  public boolean seeChance(LivingEntity l, Spawnings sp)
  {
    if (sp.chance.floatValue() > this.r.nextFloat())
    {
      return true;
    }
    return false;
  }

  public boolean seeWorld(LivingEntity l, Spawnings sp)
  {
    String[] worlds = sp.worlds.split(",");
    for (String w : worlds)
    {
      if (l.getWorld().getName().equals(w))
      {
        return true;
      }
    }
    return false;
  }

  public boolean seeBiome(LivingEntity l, Spawnings sp)
  {
    String[] Bio = sp.Biomes.split(",");
    for (String b : Bio)
    {
      if (b.equals("all"))
        return true;
      if (l.getWorld().getBiome(l.getLocation().getBlockX(), l.getLocation().getBlockZ()).equals(Biome.valueOf(b)))
      {
        return true;
      }
    }
    return false;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Spawning.SpawnListener
 * JD-Core Version:    0.6.2
 */