package me.ThaH3lper.com.Spawning;

import java.util.List;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.LoadBosses.LoadBoss;
import me.ThaH3lper.com.SaveLoad;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;

public class LoadSpawnings
{
  EpicBoss eb;

  public LoadSpawnings(EpicBoss eb)
  {
    this.eb = eb;
    LoadSpawning();
  }

  public void LoadSpawning() {
    if (this.eb.Spawning.getCustomConfig().contains("Spawning"))
    {
      for (String name : this.eb.Spawning.getCustomConfig().getConfigurationSection("Spawning").getKeys(false))
      {
        String replace = null; String worlds = null; String Biomes = null;

        Float chance = null;
        boolean remove = false;
        int limit = 0;

        if (getLoadBoss(this.eb.Spawning.getCustomConfig().getString("Spawning." + name + ".Boss")) != null)
        {
          LoadBoss loadBoss = getLoadBoss(this.eb.Spawning.getCustomConfig().getString("Spawning." + name + ".Boss"));

          if (this.eb.Spawning.getCustomConfig().contains("Spawning." + name + ".MobReplace"))
          {
            replace = this.eb.Spawning.getCustomConfig().getString("Spawning." + name + ".MobReplace");
          }
          if (this.eb.Spawning.getCustomConfig().contains("Spawning." + name + ".Chance"))
          {
            chance = Float.valueOf((float)this.eb.Spawning.getCustomConfig().getDouble("Spawning." + name + ".Chance"));
          }
          if (this.eb.Spawning.getCustomConfig().contains("Spawning." + name + ".Worlds"))
          {
            worlds = this.eb.Spawning.getCustomConfig().getString("Spawning." + name + ".Worlds");
          }
          if (this.eb.Spawning.getCustomConfig().contains("Spawning." + name + ".Biomes"))
          {
            Biomes = this.eb.Spawning.getCustomConfig().getString("Spawning." + name + ".Biomes");
          }
          if (this.eb.Spawning.getCustomConfig().contains("Spawning." + name + ".RemoveMob"))
          {
            remove = this.eb.Spawning.getCustomConfig().getBoolean("Spawning." + name + ".RemoveMob");
          }
          if (this.eb.Spawning.getCustomConfig().contains("Spawning." + name + ".Limit"))
          {
            limit = this.eb.Spawning.getCustomConfig().getInt("Spawning." + name + ".Limit");
          }

          this.eb.SpawningsList.add(new Spawnings(loadBoss, replace, chance, worlds, remove, limit, Biomes));
        }
      }
    }
  }

  public LoadBoss getLoadBoss(String s)
  {
    for (LoadBoss lb : this.eb.BossLoadList)
    {
      if (lb.getName().equals(s))
      {
        return lb;
      }
    }
    return null;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Spawning.LoadSpawnings
 * JD-Core Version:    0.6.2
 */