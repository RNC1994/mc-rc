package me.ThaH3lper.com.Spawning;

import me.ThaH3lper.com.LoadBosses.LoadBoss;

public class Spawnings
{
  public String Replace;
  public String worlds;
  public String Biomes;
  public LoadBoss loadBoss;
  public Float chance;
  public boolean remove;
  public int limit;

  public Spawnings(LoadBoss loadBoss, String Replace, Float chance, String worlds, boolean remove, int limit, String Biomes)
  {
    this.Replace = Replace;
    this.worlds = worlds;
    this.Biomes = Biomes;
    this.chance = chance;
    this.loadBoss = loadBoss;
    this.remove = remove;
    this.limit = limit;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Spawning.Spawnings
 * JD-Core Version:    0.6.2
 */