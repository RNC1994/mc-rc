package me.ThaH3lper.com.Api;

import java.util.List;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.Boss.BossCalculations;
import me.ThaH3lper.com.EpicBoss;
import org.bukkit.entity.LivingEntity;

public class Api
{
  public EpicBoss eb;

  public Api(EpicBoss neweb)
  {
    this.eb = neweb;
  }

  public boolean isBoss(LivingEntity l)
  {
    if (this.eb.bossCalculator.isBossLiv(l))
    {
      return true;
    }
    return false;
  }

  public String getBossName(LivingEntity l)
  {
    Boss b = this.eb.bossCalculator.getBoss(l);
    return b.getName();
  }

  public int getMaxHealth(LivingEntity l)
  {
    Boss b = this.eb.bossCalculator.getBoss(l);
    return b.getMaxHealth();
  }

  public int getHealth(LivingEntity l)
  {
    Boss b = this.eb.bossCalculator.getBoss(l);
    return b.getHealth();
  }

  public boolean isShowingHp(LivingEntity l)
  {
    Boss b = this.eb.bossCalculator.getBoss(l);
    return b.getShowHp();
  }

  public void addNewSkill(String name)
  {
    this.eb.CustomSkills.add(name);
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Api.Api
 * JD-Core Version:    0.6.2
 */