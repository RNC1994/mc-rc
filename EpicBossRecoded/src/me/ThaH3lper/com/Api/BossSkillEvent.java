package me.ThaH3lper.com.Api;

import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class BossSkillEvent extends Event
{
  private EpicBoss eb;
  private String name;
  private Boolean custom;
  private Boss boss;
  private static final HandlerList handlers = new HandlerList();

  public BossSkillEvent(EpicBoss neb, Boss b, String nname, Boolean ncustom)
  {
    this.eb = neb;
    this.boss = b;
    this.name = nname;
    this.custom = ncustom;
  }

  public String getBossName() {
    return this.boss.getName();
  }

  public String getSkillName() {
    return this.name;
  }

  public boolean isCustomSkill() {
    return this.custom.booleanValue();
  }

  public LivingEntity getBoss() {
    return this.boss.getLivingEntity();
  }

  public HandlerList getHandlers() {
    return handlers;
  }

  public static HandlerList getHandlerList() {
    return handlers;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Api.BossSkillEvent
 * JD-Core Version:    0.6.2
 */