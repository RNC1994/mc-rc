package me.ThaH3lper.com.Api;

import java.util.List;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class BossDeathEvent extends Event
{
  private EpicBoss eb;
  private Player player;
  private Boss boss;
  private List<ItemStack> Drops;
  private int exp;
  private static final HandlerList handlers = new HandlerList();

  public BossDeathEvent(EpicBoss neb, Player p, Boss b, List<ItemStack> nlist, int nexp)
  {
    this.eb = neb;
    this.exp = nexp;
    this.player = p;
    this.boss = b;
    this.Drops = nlist;
  }

  public String getBossName() {
    return this.boss.getName();
  }

  public Player getPlayer() {
    return this.player;
  }

  public List<ItemStack> getDrops() {
    return this.Drops;
  }

  public int getExp() {
    return this.exp;
  }
  public void setExp(int i) {
    this.exp = i;
  }

  public HandlerList getHandlers() {
    return handlers;
  }

  public static HandlerList getHandlerList() {
    return handlers;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Api.BossDeathEvent
 * JD-Core Version:    0.6.2
 */