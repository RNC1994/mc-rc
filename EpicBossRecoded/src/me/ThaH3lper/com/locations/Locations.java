package me.ThaH3lper.com.locations;

import org.bukkit.Location;

public class Locations
{
  private Location location;
  private String name;
  private String World;

  public Locations(Location loc, String newname, String worldname)
  {
    this.location = loc;
    this.name = newname;
    this.World = worldname;
  }

  public Location getLocation()
  {
    return this.location;
  }

  public String getName()
  {
    return this.name;
  }

  public String getWorldName()
  {
    return this.World;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.locations.Locations
 * JD-Core Version:    0.6.2
 */