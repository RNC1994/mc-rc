package me.ThaH3lper.com.locations;

import java.util.ArrayList;
import java.util.List;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.SaveLoad;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Server;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

public class LocationStuff
{
  EpicBoss eb;

  public LocationStuff(EpicBoss neweb)
  {
    this.eb = neweb;
    loadLocation();
    saveLocation();
  }

  public void addLocation(String name, Location l)
  {
    this.eb.LocationList.add(new Locations(l, name, l.getWorld().getName()));
    saveLocation();
  }

  public void removeLocation(String name)
  {
    if (this.eb.LocationList != null)
    {
      int i = 0;
      while (i < this.eb.LocationList.size())
      {
        if (((Locations)this.eb.LocationList.get(i)).getName().equals(name))
        {
          this.eb.LocationList.remove(i);
          saveLocation();
        }
        i++;
      }
    }
  }

  public void loadLocation()
  {
    if (this.eb.SavedData.getCustomConfig().contains("Location"))
    {
      if (this.eb.SavedData.getCustomConfig().getStringList("Location") != null)
      {
        for (String s : this.eb.SavedData.getCustomConfig().getStringList("Location"))
        {
          String[] Splits = s.split(":");

          String name = Splits[0];
          Location l = new Location(Bukkit.getWorld(Splits[1]), Double.parseDouble(Splits[2]), Double.parseDouble(Splits[3]), Double.parseDouble(Splits[4]));

          this.eb.LocationList.add(new Locations(l, name, Splits[1]));
        }
      }
    }
  }

  public void saveLocation()
  {
    if (this.eb.LocationList != null)
    {
      List saved = new ArrayList();
      for (Locations loc : this.eb.LocationList)
      {
        String save = loc.getName() + ":" + loc.getWorldName() + ":" + (int)loc.getLocation().getX() + ":" + (int)loc.getLocation().getY() + ":" + (int)loc.getLocation().getZ();
        saved.add(save);
      }
      this.eb.SavedData.reloadCustomConfig();
      this.eb.SavedData.getCustomConfig().set("Location", saved);
      this.eb.SavedData.saveCustomConfig();
    }
  }

  public boolean locationExict(String name) {
    if (this.eb.LocationList != null)
    {
      for (Locations loc : this.eb.LocationList)
      {
        if (loc.getName().equals(name))
        {
          return true;
        }
      }
    }
    return false;
  }

  public Player getPlayer(String name) {
    for (Player p : Bukkit.getServer().getOnlinePlayers())
    {
      if (p.getName().equals(name))
      {
        return p;
      }
    }
    return null;
  }

  public Locations getLocations(String name) {
    if (this.eb.LocationList != null)
    {
      for (Locations loc : this.eb.LocationList)
      {
        if (loc.getName().equals(name))
        {
          return loc;
        }
      }
    }
    return null;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.locations.LocationStuff
 * JD-Core Version:    0.6.2
 */