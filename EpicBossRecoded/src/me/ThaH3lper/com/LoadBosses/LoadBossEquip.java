package me.ThaH3lper.com.LoadBosses;

import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.EntityEquipment;
import org.bukkit.inventory.ItemStack;

public class LoadBossEquip
{
  public EpicBoss eb;

  public LoadBossEquip(EpicBoss boss)
  {
    this.eb = boss;
  }

  public void SetEqupiment(Boss b) {
    if (b.getItems() != null)
    {
      if (b.getLivingEntity() != null)
      {
        for (String s : b.getItems())
        {
          String[] parts = s.split(" ");
          if ((!parts[0].equals("exp")) && (!parts[0].equals("hexp")))
            equip(s, this.eb.loaditems.getItem(s), b.getLivingEntity());
        }
      }
    }
  }

  public void equip(String s, ItemStack stack, LivingEntity l) {
    String[] parts = s.split(" ");
    String[] itemParts = parts[0].split(":");
    if (itemParts.length == 4)
    {
      EntityEquipment eq = l.getEquipment();

      if (itemParts[3].equals("0")) {
        eq.setItemInHand(stack);
      }
      if (itemParts[3].equals("1")) {
        eq.setBoots(stack);
      }
      if (itemParts[3].equals("2")) {
        eq.setLeggings(stack);
      }
      if (itemParts[3].equals("3")) {
        eq.setChestplate(stack);
      }
      if (itemParts[3].equals("4"))
        eq.setHelmet(stack);
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.LoadBosses.LoadBossEquip
 * JD-Core Version:    0.6.2
 */