package me.ThaH3lper.com.LoadBosses;

import java.util.ArrayList;
import java.util.List;

public class LoadBoss
{
  public String Name;
  public String Type;
  public int Health;
  public int Damage;
  boolean Showhp;
  public List<String> Items;
  public List<String> Skillslist;
  boolean showtitle = false;
  String skin;

  public LoadBoss(String newName, String newType, int newHealth, int newDamage, List<String> newItems, boolean newShowhp, List<String> newskills, String Skin, Boolean Showtitle)
  {
    this.Name = newName;
    this.Type = newType;
    this.Health = newHealth;
    this.Damage = newDamage;
    this.Items = new ArrayList(newItems);
    this.Showhp = newShowhp;
    this.Skillslist = new ArrayList(newskills);
    this.skin = Skin;
    this.showtitle = Showtitle.booleanValue();
  }

  public String getName() {
    return this.Name;
  }

  public String getType() {
    return this.Type;
  }

  public int getHealth() {
    return this.Health;
  }

  public int getDamage() {
    return this.Damage;
  }

  public List<String> getItems() {
    return this.Items;
  }

  public List<String> getSkills() {
    return this.Skillslist;
  }

  public boolean getShowhp() {
    return this.Showhp;
  }

  public boolean getShowtitle() {
    return this.showtitle;
  }

  public String getSkin() {
    return this.skin;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.LoadBosses.LoadBoss
 * JD-Core Version:    0.6.2
 */