package me.ThaH3lper.com.LoadBosses;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class DropItems
{
  Random r = new Random();
  EpicBoss eb;

  public DropItems(EpicBoss neb)
  {
    this.eb = neb;
  }

  public void dropItems(List<ItemStack> items, Boss b, int exp) {
    for (ItemStack s : items)
    {
      b.getLocation().getWorld().dropItem(b.getLocation(), s);
    }
    if (exp != 0)
    {
      int i = 0;
      int x = exp / 20;
      while (i < 20)
      {
        ExperienceOrb ob = (ExperienceOrb)b.getLocation().getWorld().spawnEntity(b.getLocation(), EntityType.EXPERIENCE_ORB);
        ob.setExperience(x + 1);
        i++;
      }
    }
  }

  public List<ItemStack> getDrops(Boss b) {
    List items = new ArrayList();
    if (b.getItems() != null)
    {
      for (String s : b.getItems())
      {
        String[] part = s.split(" ");
        if ((!part[0].equalsIgnoreCase("exp")) && (!part[0].equalsIgnoreCase("hexp")))
        {
          ItemStack sta = this.eb.dropitems.getDropsItems(this.eb.loaditems.getItem(s), this.eb.loaditems.getItemChance(s), this.eb.loaditems.getDisplayName(s));
          if (sta != null)
          {
            items.add(sta);
          }
        }
      }
    }
    return items;
  }

  public int getExp(Boss b) {
    if (b.getItems() != null)
    {
      for (String s : b.getItems())
      {
        String[] part = s.split(" ");
        if (part[0].equalsIgnoreCase("exp"))
        {
          int i = Integer.parseInt(part[1]);
          return i;
        }
      }
    }
    return 0;
  }

  public int getHeroesExp(Boss b) {
    if (b.getItems() != null)
    {
      for (String s : b.getItems())
      {
        String[] part = s.split(" ");
        if (part[0].equalsIgnoreCase("hexp"))
        {
          int i = Integer.parseInt(part[1]);
          return i;
        }
      }
    }
    return 0;
  }

  public ItemStack getDropsItems(ItemStack stack, float f, String s) {
    if (s != null)
    {
      ItemMeta stackMeta = stack.getItemMeta();
      stackMeta.setDisplayName(s);
      stack.setItemMeta(stackMeta);
    }
    if (this.r.nextFloat() <= f) {
      return stack;
    }
    return null;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.LoadBosses.DropItems
 * JD-Core Version:    0.6.2
 */