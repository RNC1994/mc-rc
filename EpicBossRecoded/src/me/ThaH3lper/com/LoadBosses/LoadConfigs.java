package me.ThaH3lper.com.LoadBosses;

import java.util.ArrayList;
import java.util.List;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.SaveLoad;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.LivingEntity;

public class LoadConfigs
{
  public EpicBoss eb;

  public LoadConfigs(EpicBoss boss)
  {
    this.eb = boss;
    LoadBosses();
  }

  public void LoadBosses() {
    this.eb.BossLoadList.clear();
    this.eb.Bosses.reloadCustomConfig();
    if (this.eb.Bosses.getCustomConfig().contains("Bosses"))
    {
      for (String name : this.eb.Bosses.getCustomConfig().getConfigurationSection("Bosses").getKeys(false))
      {
        String Skin = null;

        List Items = new ArrayList(); List Skills = new ArrayList();

        boolean showtitle = false;

        String Name = name;
        String Type = this.eb.Bosses.getCustomConfig().getString("Bosses." + name + ".Type");
        int Health = this.eb.Bosses.getCustomConfig().getInt("Bosses." + name + ".Health");
        int Damage = this.eb.Bosses.getCustomConfig().getInt("Bosses." + name + ".Damage");
        if (this.eb.Bosses.getCustomConfig().contains("Bosses." + name + ".Drops"))
        {
          Items = this.eb.Bosses.getCustomConfig().getStringList("Bosses." + name + ".Drops");
        }
        if (this.eb.Bosses.getCustomConfig().contains("Bosses." + name + ".Skills"))
        {
          Skills = this.eb.Bosses.getCustomConfig().getStringList("Bosses." + name + ".Skills");
        }
        if (this.eb.Bosses.getCustomConfig().contains("Bosses." + name + ".Skin"))
        {
          Skin = this.eb.Bosses.getCustomConfig().getString("Bosses." + name + ".Skin");
        }
        if (this.eb.Bosses.getCustomConfig().contains("Bosses." + name + ".Showtitle"))
        {
          showtitle = this.eb.Bosses.getCustomConfig().getBoolean("Bosses." + name + ".Showtitle");
        }
        boolean showhp = this.eb.Bosses.getCustomConfig().getBoolean("Bosses." + name + ".Showhp");

        this.eb.BossLoadList.add(new LoadBoss(Name, Type, Health, Damage, Items, showhp, Skills, Skin, Boolean.valueOf(showtitle)));
      }
    }
  }

  public LoadBoss getLoadBoss(String s) {
    if (this.eb.BossLoadList != null)
    {
      for (LoadBoss lb : this.eb.BossLoadList)
      {
        if (lb.getName().equals(s))
        {
          return lb;
        }
      }
    }
    return null;
  }

  public void SaveAllBosses() {
    if (this.eb.BossList != null)
    {
      List saved = new ArrayList();
      for (Boss boss : this.eb.BossList)
      {
        if (!boss.getNatural())
        {
          if (boss.getTimer().equals("null"))
          {
            String save = boss.getName() + ":" + boss.getHealth() + ":" + boss.getWorkingLocation().getWorld().getName() + ":" + boss.getWorkingLocation().getBlockX() + ":" + boss.getWorkingLocation().getBlockY() + ":" + boss.getWorkingLocation().getBlockZ() + ":" + boss.getTimer();
            saved.add(save);
          }
          if (!boss.getSaved())
          {
            boss.getLivingEntity().remove();
          }
        }
      }
      this.eb.SavedData.reloadCustomConfig();
      this.eb.SavedData.getCustomConfig().set("Bosses", saved);
      this.eb.SavedData.saveCustomConfig();
    }
  }

  public void LoadAllBosses()
  {
    if (this.eb.SavedData.getCustomConfig().contains("Bosses"))
    {
      if (this.eb.SavedData.getCustomConfig().getStringList("Bosses") != null)
      {
        for (String s : this.eb.SavedData.getCustomConfig().getStringList("Bosses"))
        {
          String[] Splits = s.split(":");
          if (getLoadBoss(Splits[0]) != null)
          {
            LoadBoss lb = getLoadBoss(Splits[0]);
            Location l = new Location(Bukkit.getWorld(Splits[2]), Integer.parseInt(Splits[3]), Integer.parseInt(Splits[4]), Integer.parseInt(Splits[5]));

            Boss bs = new Boss(lb.getName(), lb.getHealth(), l, lb.getType(), lb.getDamage(), lb.getShowhp(), lb.getItems(), lb.getSkills(), lb.getShowtitle(), lb.getSkin());
            bs.sethealth(Integer.parseInt(Splits[1]));
            if (!Splits[6].equals("null"));
            bs.setTimer(Splits[6]);

            this.eb.BossList.add(bs);
          }
        }
      }
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.LoadBosses.LoadConfigs
 * JD-Core Version:    0.6.2
 */