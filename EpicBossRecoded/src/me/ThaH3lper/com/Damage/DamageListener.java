package me.ThaH3lper.com.Damage;

import com.herocraftonline.heroes.Heroes;
import com.herocraftonline.heroes.characters.CharacterManager;
import com.herocraftonline.heroes.characters.Hero;
import java.util.List;
import me.ThaH3lper.com.Api.BossDeathEvent;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.Boss.BossCalculations;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.LoadBosses.DropItems;
import me.ThaH3lper.com.Skills.SkillsHandler;
import me.ThaH3lper.com.Timer.Spawn.Despawn;
import me.ThaH3lper.com.Timer.TimerSeconds;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.SmallFireball;
import org.bukkit.entity.Snowball;
import org.bukkit.entity.ThrownPotion;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityCombustEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.EntityTameEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitScheduler;

public class DamageListener
  implements Listener
{
  private EpicBoss eb;
  BossDeathEvent event;

  public DamageListener(EpicBoss boss)
  {
    this.eb = boss;
  }

  @EventHandler(priority=EventPriority.HIGH)
  public void Damage(EntityDamageByEntityEvent e)
  {
    if (!this.eb.HeroesEnabled)
    {
      if (DamageMethod(e.getDamager(), e.getEntity(), e.getDamage()))
      {
        e.setCancelled(true);
      }
      if (this.eb.bossCalculator.isBoss(e.getDamager()))
      {
        Boss boss = this.eb.bossCalculator.getBoss(e.getDamager());
        e.setDamage(boss.getDamage());
      }
      else if (this.eb.bossCalculator.isBoss(e.getEntity()))
      {
        e.setDamage(1);
      }
    }
  }

  @EventHandler(priority=EventPriority.HIGH)
  public void BossNoLose(EntityDamageEvent event) {
    if (event.getEntity() != null)
    {
      if ((this.eb.bossCalculator.isBoss(event.getEntity())) && 
        (event.getCause() != EntityDamageEvent.DamageCause.ENTITY_ATTACK) && (event.getCause() != EntityDamageEvent.DamageCause.PROJECTILE) && (event.getCause() != EntityDamageEvent.DamageCause.MAGIC) && (event.getCause() != EntityDamageEvent.DamageCause.CUSTOM))
      {
        event.setCancelled(true);
      }
    }
  }

  @EventHandler(priority=EventPriority.HIGH)
  public void BossNoFire(EntityCombustEvent e)
  {
    if (this.eb.bossCalculator.isBoss(e.getEntity()))
    {
      e.setCancelled(true);
    }
  }

  @EventHandler(priority=EventPriority.HIGH)
  public void NoTaming(EntityTameEvent e) {
    if (this.eb.bossCalculator.isBoss(e.getEntity()))
    {
      e.setCancelled(true);
    }
  }

  @EventHandler(priority=EventPriority.HIGH)
  public void NoBlowCreeper(EntityExplodeEvent e) {
    this.eb.getServer().getScheduler().scheduleSyncDelayedTask(this.eb, new Runnable() {
      public void run() { DamageListener.this.eb.timer.despawn.DeSpawnEvent(DamageListener.this.eb); }

    }
    , 1L);
  }

  public boolean DamageMethod(Entity Damager, Entity Hited, int damage)
  {
    if ((Damager instanceof Arrow))
    {
      Arrow a = (Arrow)Damager;
      Damager = a.getShooter();
      if ((Damager == null) && (this.eb.bossCalculator.isBoss(Hited)))
      {
        return true;
      }
      if (Damager == null)
      {
        return false;
      }
    }
    if ((Damager instanceof Fireball))
    {
      Fireball a = (Fireball)Damager;
      Damager = a.getShooter();
    }
    if ((Damager instanceof SmallFireball))
    {
      SmallFireball a = (SmallFireball)Damager;
      Damager = a.getShooter();
    }
    if ((Damager instanceof Snowball))
    {
      Snowball a = (Snowball)Damager;
      Damager = a.getShooter();
    }
    if ((Damager instanceof Egg))
    {
      Egg a = (Egg)Damager;
      Damager = a.getShooter();
    }
    if ((Damager instanceof ThrownPotion))
    {
      ThrownPotion a = (ThrownPotion)Damager;
      Damager = a.getShooter();
    }
    if (((Damager instanceof LivingEntity)) && ((Hited instanceof LivingEntity)))
    {
      if ((!this.eb.bossCalculator.isBoss(Damager)) && (!this.eb.bossCalculator.isBoss(Hited)))
      {
        return false;
      }

      LivingEntity hited = (LivingEntity)Hited;
      if (this.eb.bossCalculator.isBoss(Hited))
      {
        if (!this.eb.bossCalculator.BossHited(hited).booleanValue())
        {
          Boss boss = this.eb.bossCalculator.getBoss(Hited);
          boss.sethealth(boss.getHealth() - damage);
          hited.setHealth(hited.getMaxHealth());
          if ((Damager instanceof Player))
          {
            this.eb.skillhandler.skills(boss, (Player)Damager);
          }
          if (boss.getHealth() <= 0)
          {
            List dropItems = this.eb.dropitems.getDrops(boss);

            int exp = this.eb.dropitems.getExp(boss);
            int hexp = this.eb.dropitems.getHeroesExp(boss);
            if (this.eb.HeroesEnabled)
            {
              if ((Damager instanceof Player))
              {
                this.eb.heroes.getCharacterManager().getHero((Player)Damager).addExp(hexp, this.eb.heroes.getCharacterManager().getHero((Player)Damager).getHeroClass(), boss.getLocation());
              }
            }

            if ((Damager instanceof Player))
            {
              this.event = new BossDeathEvent(this.eb, (Player)Damager, boss, dropItems, exp);
              Bukkit.getServer().getPluginManager().callEvent(this.event);
            }
            this.eb.damagemethods.deathBoss(boss, dropItems, this.event.getExp());
          }
          else if (boss.getShowHp())
          {
            if ((Damager instanceof Player))
            {
              Player p = (Player)Damager;
              if (!this.eb.percentage)
              {
                String bossName = boss.getName();
                bossName = bossName.replace("_", " ");
                String s = this.eb.name + ChatColor.RED + bossName + ChatColor.GRAY + " [" + ChatColor.DARK_RED + boss.getHealth() + ChatColor.GRAY + "/" + ChatColor.DARK_RED + boss.getMaxHealth() + ChatColor.GRAY + "]";
                p.sendMessage(s);
              }
              else
              {
                double per = boss.getHealth() / boss.getMaxHealth() * 10.0D + 1.0D;
                if (!boss.hasPercent((int)per))
                {
                  String bossName = boss.getName();
                  bossName = bossName.replace("_", " ");
                  String s = this.eb.name + ChatColor.RED + bossName + ChatColor.GRAY + " [" + ChatColor.DARK_RED + (int)per * 10 + "%" + ChatColor.GRAY + "]";
                  for (Player player : this.eb.skillhandler.getPlayersRadious(20, boss))
                  {
                    player.sendMessage(s);
                  }
                  boss.addPercent((int)per);
                }
              }
            }
          }
        }
      }
    }
    return false;
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Damage.DamageListener
 * JD-Core Version:    0.6.2
 */