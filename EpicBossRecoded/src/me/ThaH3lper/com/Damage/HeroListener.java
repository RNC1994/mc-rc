package me.ThaH3lper.com.Damage;

import com.herocraftonline.heroes.api.events.SkillDamageEvent;
import com.herocraftonline.heroes.api.events.WeaponDamageEvent;
import com.herocraftonline.heroes.characters.CharacterTemplate;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.Boss.BossCalculations;
import me.ThaH3lper.com.EpicBoss;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class HeroListener
  implements Listener
{
  private EpicBoss eb;
  private DamageListener dl;

  public HeroListener(EpicBoss boss, DamageListener dl)
  {
    this.eb = boss;
    this.dl = dl;
  }

  @EventHandler(priority=EventPriority.HIGH)
  public void HeroWeapon(WeaponDamageEvent e)
  {
    if (this.dl.DamageMethod(e.getDamager().getEntity(), e.getEntity(), e.getDamage()))
    {
      e.setCancelled(true);
    }
    if (this.eb.bossCalculator.isBoss(e.getDamager().getEntity()))
    {
      Boss boss = this.eb.bossCalculator.getBoss(e.getDamager().getEntity());
      e.setDamage(boss.getDamage());
    }
    else if (this.eb.bossCalculator.isBoss(e.getEntity()))
    {
      e.setDamage(1);
    }
  }

  @EventHandler(priority=EventPriority.HIGH)
  public void HeroSkillDamage(SkillDamageEvent e) {
    if (this.dl.DamageMethod(e.getDamager().getEntity(), e.getEntity(), e.getDamage()))
    {
      e.setCancelled(true);
    }
    if (this.eb.bossCalculator.isBoss(e.getDamager().getEntity()))
    {
      Boss boss = this.eb.bossCalculator.getBoss(e.getDamager().getEntity());
      e.setDamage(boss.getDamage());
    }
    else if (this.eb.bossCalculator.isBoss(e.getEntity()))
    {
      e.setDamage(1);
    }
  }
}

/* Location:           C:\Users\RC\Desktop\Server\plugins\EpicBossRecoded.jar
 * Qualified Name:     me.ThaH3lper.com.Damage.HeroListener
 * JD-Core Version:    0.6.2
 */