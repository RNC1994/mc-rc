package me.ThaH3lper.com.Damage;

import java.util.List;
import me.ThaH3lper.com.Boss.Boss;
import me.ThaH3lper.com.EpicBoss;
import me.ThaH3lper.com.LoadBosses.DropItems;
import me.ThaH3lper.com.Timer.TimerStuff;
import org.bukkit.entity.LivingEntity;
import org.bukkit.inventory.ItemStack;

public class DamageMethods
{
  private EpicBoss eb;

  public DamageMethods(EpicBoss boss)
  {
    this.eb = boss;
  }

  public void deathBoss(Boss b, List<ItemStack> stack, int exp) {
    this.eb.timerstuff.Death(b);
    this.eb.dropitems.dropItems(stack, b, exp);
    if (b.getLivingEntity() != null)
    {
      //b.getLivingEntity().remove();
    	b.getLivingEntity().setHealth(0);
    }
    //this.eb.BossList.remove(b);
  }
}