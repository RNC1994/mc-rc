package RC2K7.Plugins.EmtityMeteor;

import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_5_R2.CraftWorld;
import org.bukkit.craftbukkit.v1_5_R2.entity.CraftEntity;
import org.bukkit.entity.Explosive;
import org.bukkit.entity.Fireball;
import org.bukkit.event.entity.ExplosionPrimeEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.util.Vector;

import net.minecraft.server.v1_5_R2.DamageSource;
import net.minecraft.server.v1_5_R2.EntityFireball;
import net.minecraft.server.v1_5_R2.EntityLiving;
import net.minecraft.server.v1_5_R2.MovingObjectPosition;
import net.minecraft.server.v1_5_R2.World;
import net.minecraft.server.v1_5_R2.WorldServer;

public class EntityMeteor extends EntityFireball {
	
	private float SpeedMod = 1.10F;
	private float ExplosionRadius = 70F;
	private float TrailPower = 10F;
	
	public EntityMeteor(World world)
	{
		super(world);
		this.a(1.0F, 1.0F);
	}
	
	@Override
	public void l_()
	{
		final Fireball fireball = (Fireball)this.getBukkitEntity();
		fireball.getWorld().createExplosion(fireball.getLocation(), this.TrailPower);
		
		motX *= this.SpeedMod;
		motY *= this.SpeedMod;
		motZ *= this.SpeedMod;
		
		super.l_();
	}

	@Override
	protected void a(MovingObjectPosition movingobjectposition)
	{
		if(!world.isStatic)
		{
			if(movingobjectposition != null)
				movingobjectposition.entity.damageEntity(DamageSource.fireball(this, shooter), 6);
			ExplosionPrimeEvent event = new ExplosionPrimeEvent((Explosive)CraftEntity.getEntity(world.getServer(), this));
			world.getServer().getPluginManager().callEvent(event);
			if(!event.isCancelled())
			{
				world.createExplosion(this, locX, locY, locZ, this.ExplosionRadius, event.getFire(), true);
				
			}
			this.die();
		}
	}
	
	public Vector getDirection() {
	    return new Vector(this.dirX, this.dirY, this.dirZ);
	    }
	 
	    public void setDirection(Vector direction) {
	    this.setDirection(direction.getX(), direction.getY(), direction.getZ());
	    }
	 
	    public Vector getVelocity() {
	    return new Vector(this.motX, this.motY, this.motZ);
	    }
	 
	    public void setVelocity(Vector vel) {
	    this.motX = vel.getX();
	    this.motY = vel.getY();
	    this.motZ = vel.getZ();
	    this.velocityChanged = true;
	    }
	 
	    public CraftWorld getWorld() {
	    return ((WorldServer) this.world).getWorld();
	    }
	 
	    public boolean teleport(Location location) {
	    return teleport(location, TeleportCause.PLUGIN);
	    }
	 
	    public boolean teleport(Location location, TeleportCause cause) {
	    this.world = ((CraftWorld) location.getWorld()).getHandle();
	    this.setLocation(location.getX(), location.getY(), location.getZ(),
	        location.getYaw(), location.getPitch());
	    // entity.setLocation() throws no event, and so cannot be cancelled
	    return true;
	    }
	 
	    public void setYield(float yield) {
	    this.yield = yield;
	    }
	 
	    public float getYield() {
	    return this.yield;
	    }
	 
	    public void setYaw(float yaw) {
	    this.yaw = yaw;
	    }
	 
	    public float getYaw() {
	    return this.yaw;
	    }
	 
	    public void setPitch(float pitch) {
	    this.pitch = pitch;
	    }
	 
	    public float getPitch() {
	    return this.pitch;
	    }
	 
	    public void setSpeedModifier(float modifier) {
	    this.SpeedMod = modifier;
	    }
	 
	    public float getSpeedModifier(float modifier) {
	    return this.SpeedMod;
	    }
	 
	    public void setExplosionRadius(float radius) {
	    this.ExplosionRadius = radius;
	    }
	 
	    public float getExplosionRadius() {
	    return this.ExplosionRadius;
	    }
	 
	    public void setTrailPower(float power) {
	    this.TrailPower = power;
	    }
	 
	    public float getTrailPower() {
	    return this.TrailPower;
	    }
	    
	    @Override
	    public void die() {
	    }
	    
	    public EntityLiving shooter;
	    public double dirX;
	    public double dirY;
	    public double dirZ;
	    public float yield;
	    public boolean isIncendiary;

}
